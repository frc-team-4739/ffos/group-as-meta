# Group as Meta

Converts groups to a meta package. This is important as groups cannot be a dependency of another package, but meta packages can.